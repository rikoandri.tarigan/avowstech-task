package avowstech.api.task;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncoderTest {

	@Test
	public void encode() {
		PasswordEncoder p = new BCryptPasswordEncoder();
		System.out.println(p.encode("123456"));
	}
}
