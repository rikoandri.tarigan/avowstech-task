package avowstech.api.task.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import avowstech.api.task.Models.User;

public interface UserRepository extends JpaRepository<User, Long>{
	@Query(value = "SELECT * FROM `user` WHERE 1 ORDER BY ID DESC",nativeQuery=true)
    public List<User> findUserOrderById();

	Optional<User> findByUsername(String username);
}
