package avowstech.api.task.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import avowstech.api.task.Models.Authority;
import avowstech.api.task.Models.User;

public interface AuthorityRepository extends JpaRepository<Authority, Long>{

}
