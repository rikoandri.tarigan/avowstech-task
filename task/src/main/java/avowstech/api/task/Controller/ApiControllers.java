package avowstech.api.task.Controller;

import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import avowstech.api.task.Models.Authority;
import avowstech.api.task.Models.User;
import avowstech.api.task.Repository.AuthorityRepository;
import avowstech.api.task.Repository.UserRepository;
import ch.qos.logback.core.pattern.color.BoldCyanCompositeConverter;
import net.bytebuddy.matcher.CollectionOneToOneMatcher;

@RestController
public class ApiControllers {
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private AuthorityRepository authRepo;
	
	@GetMapping(value="/addsomeuser")
	public List<User> addSomeUser() {
		
		List<User> users = new ArrayList<>();
		
		User user ;
		for (int i = 0; i < 5; i++) {
			user = new User();
			user.setFirstName("john"+i);
			user.setLastName("Doe"+i);
			user.setAge(26);
			user.setOccupation("Junior Developer");
			user.setUsername(("johndoe"+i).toString());
			user.setPassword(new BCryptPasswordEncoder().encode(("johndoe")+i).toString());
			users.add(user);
		}
		
		Stream<User> stream = users.stream();
		stream.forEach(p -> userRepo.save(p));
		
		Authority auth;
		User usr ;
		for (int i = 0; i < 5; i++) {
			usr = null;
			usr = userRepo.findByUsername("johndoe"+i).get();
			auth = new Authority();
			if(i==0) { //FIRST DATA SET AUTHORITY TO ADMIN
				auth.setAuthority("ADMIN");
			}else if(i==1) { //FIRST DATA SET AUTHORITY TO GUEST
				auth.setAuthority("GUEST");
			}else {
				auth.setAuthority("USER");
			}
			auth.setUser(usr);
			authRepo.save(auth);
		}
		
		return users;
	}
	
	@GetMapping("/")
	public String getPage() {
		return "Index Page";
	}
	@GetMapping("/user")
	public String getPageuser() {
		return "welcom user";
	}
	@GetMapping("/admin")
	public String getPageadmin() {
		return "welcom admin";
	}
	
	@GetMapping(value="/users")
	public List<User> getUsers() {
		return userRepo.findUserOrderById();
	}
	
	@PostMapping(value="saveUser")
	public String saveUser(@RequestBody User user) {
		userRepo.save(user);
		return "saved ...";
	}
	
	@PutMapping(value="update/{id}")
	public String updateUser(@PathVariable long id,@RequestBody User user) {
		User updateUser = userRepo.findById(id).get();
		updateUser.setFirstName(user.getFirstName());
		updateUser.setLastName(user.getLastName());
		updateUser.setAge(user.getAge());
		updateUser.setOccupation(user.getOccupation());
		userRepo.save(updateUser);
		
		return "updated ...";
	}
	
	@DeleteMapping(value="delete/{id}")
	public String deleteUser(@PathVariable long id) {
		User deleteUser = userRepo.findById(id).get();
		userRepo.delete(deleteUser);
		return "Delete user with id:"+id;
	}
}
